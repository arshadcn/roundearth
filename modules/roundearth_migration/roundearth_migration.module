<?php

/**
 * @file
 * Hook implementations for RoundEarth Migration module.
 */

use Drupal\roundearth_migration\Plugin\migrate\source\d6\Node as CustomNodeD6;
use Drupal\roundearth_migration\Plugin\migrate\source\d7\Node as CustomNodeD7;
use Drupal\roundearth_migration\Plugin\migrate\source\d6\Comment as CustomCommentD6;
use Drupal\roundearth_migration\Plugin\migrate\source\d6\User as CustomUserD6;

/**
 * Implements hook_migrate_source_info_alter().
 */
function roundearth_migration_migrate_source_info_alter(&$definitions) {
  if (isset($definitions['d6_node'])) {
    $definitions['d6_node']['class'] = CustomNodeD6::class;
  }
  if (isset($definitions['d7_node'])) {
    $definitions['d7_node']['class'] = CustomNodeD7::class;
  }
  if (isset($definitions['d6_comment'])) {
    $definitions['d6_comment']['class'] = CustomCommentD6::class;
  }
  if (isset($definitions['d6_user'])) {
    $definitions['d6_user']['class'] = CustomUserD6::class;
  }
}

/**
 * Implements hook_migration_plugins_alter().
 */
function roundearth_migration_migration_plugins_alter(&$definitions) {
  foreach ($definitions as $name => &$definition) {
    if (preg_match('/^d([67])_node(_revision)?:/', $name, $matches)) {
      $version = $matches[1];

      // Use our text format processor by default.
      $default_text_format_processor = [
        'plugin' => 'roundearth_migration_text_formats',
        'source' => 'format',
      ];
      if (isset($definition['process']['body/format'])) {
        $definition['process']['body/format'] = $default_text_format_processor;
      }
      if (isset($definition['process']['body']['format'])) {
        $definition['process']['body']['format'] = $default_text_format_processor;
      }
    }
  }
}
