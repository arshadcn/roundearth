<?php

namespace Drupal\roundearth_migration\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Pipeline Processor configuration.
 *
 * @ConfigEntityType(
 *   id = "pipeline_processor",
 *   label = @Translation("Pipeline processor"),
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "process"
 *   }
 * )
 */
class PipelineProcessor extends ConfigEntityBase {

  /**
   * Runs a value through a migration pipeline process.
   *
   * @param mixed $value
   *   The initial value.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration in which this process is being executed.
   * @param \Drupal\migrate\Row $row
   *   The row from the source to process.
   * @param string $destination_property
   *   The destination property currently worked on.
   *
   * @return mixed|null
   *   The result of the pipeline process.
   *
   * @throws \Drupal\migrate\MigrateException
   */
  public function process($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $process = $this->get('process');
    $values = $row->getSource();
    $values['process'] = $value;
    $new_row = new Row($values, $row->getSourceIdValues());
    $new_row->freezeSource();
    $migrate_executable->processRow($new_row, [$destination_property => $process], $value);
    return $new_row->getDestinationProperty($destination_property);
  }

}
