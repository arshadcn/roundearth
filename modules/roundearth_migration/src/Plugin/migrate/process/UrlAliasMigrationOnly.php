<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\Core\Database\Connection;
use Drupal\Core\Path\AliasStorage;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\roundearth_migration\MigrateMapUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UrlMigrationOnly.
 *
 * Removes aliases for the given source that were not performed via the
 * specified migrations.
 *
 * Configuration:
 *
 * - source: Drupal 8 internal path.
 * - migrations: an array of migrations to analyze to determine if the alias
 *     was obtained via a migration.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_url_alias_migration_only"
 * )
 */
class UrlAliasMigrationOnly extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $aliasStorage;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\roundearth_migration\MigrateMapUtil
   */
  protected $mapUtil;

  /**
   * Constructs a UrlAliasMigrationOnly.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Path\AliasStorageInterface $alias_storage
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\roundearth_migration\MigrateMapUtil $map_util
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AliasStorageInterface $alias_storage, Connection $database, MigrateMapUtil $map_util) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->aliasStorage = $alias_storage;
    $this->database = $database;
    $this->mapUtil = $map_util;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.alias_storage'),
      $container->get('database'),
      $container->get('roundearth_migration.migrate_map_util')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Get aliases with the same source.
    $aliasIds = $this->getAliasIds($value);
    $this->pruneAliases($aliasIds);
    return $value;
  }

  /**
   * Gets the IDs of aliases for the given source.
   *
   * @param string $source
   *   The alias source (internal path).
   *
   * @return array
   *   The alias IDs.
   */
  protected function getAliasIds($source) {
    $query = $this->database->select(AliasStorage::TABLE)
      ->fields(AliasStorage::TABLE, ['pid'])
      ->condition('source', $source);
    return $query->execute()->fetchCol();
  }

  /**
   * Prunes the aliases to only migrated aliases.
   *
   * @param array $ids
   *   The alias IDs.
   */
  protected function pruneAliases(array $ids) {
    $migrations = $this->configuration['migrations'];
    foreach ($ids as $id) {
      if (!$this->mapUtil->lookupSourceId($id, $migrations)) {
        $this->aliasStorage->delete(['pid' => $id]);
      }
    }
  }

}
