<?php

use Drupal\Core\Url;

/**
 * Implements template_preprocess_civicrm_entity().
 */
function pangea_preprocess_civicrm_entity(&$variables) {
  $entity_type = $variables['entity_type'];

  switch ($entity_type) {
    case 'civicrm_contact':
      /** @var \Drupal\civicrm_entity\Entity\CivicrmEntity $contact */
      $contact = $variables['elements']['#civicrm_contact'];

      // Get the bio from the custom field.
      if ($bio = roundearth_events_get_custom_field_value_for_entity('roundearth_events_bio', $contact)) {
        $variables['content']['bio'] = [
          '#markup' => $bio,
        ];
      }
      break;

    case 'civicrm_event':
      /** @var \Drupal\Core\Entity\EntityInterface $event */
      $event = $variables['elements']['#civicrm_event'];

      // Make civicrm fields renderable in the template.
      $civicrm_fields = [
        'summary',
        'start_date',
        'end_date',
        'description',
        'is_online_registration',
        'registration_link_text',
        'registration_start_date',
        'registration_end_date',
        'intro_text',
        'footer_text',
      ];

      $civicrm_entity_settings = \Drupal::configFactory()
        ->get('civicrm_entity.settings');

      foreach ($civicrm_fields as $field_name) {
        if ($event->hasField($field_name)) {
          $field = $event->get($field_name);
          $field_type = $field->getFieldDefinition()->getType();
          $value = $field->value;

          // Format fields.
          switch ($field_type) {
            case 'text_long':
              $value = [
                '#type' => 'processed_text',
                '#text' => $value,
                '#format' => $civicrm_entity_settings->get('filter_format') ?: filter_fallback_format(),
              ];
              break;
            case 'datetime':
              if ($date = $field->date) {
                $value = Drupal::service('date.formatter')
                  ->format($date->getTimestamp(), 'event_date_medium');
              }
              break;
          }

          $variables['content'][$field_name] = $value;
        }
      }

      // Add an upcoming variables.
      $variables['content']['upcoming'] = TRUE;
      if (($event->hasField('start_date')) && ($start_date = $event->get('start_date')->value)) {
        if (strtotime($start_date) < time()) {
          $variables['content']['upcoming'] = FALSE;
        }
      }

      if (($event->hasField('end_date')) && ($end_date = $event->get('end_date')->value)) {
        if (strtotime($end_date) > time()) {
          $variables['content']['upcoming'] = TRUE;
        }
      }

      // Generate a link for online registration.
      $variables['content']['online_registration_url'] = Url::fromRoute('civicrm.civicrm_event_register', [], [
        'query' => [
          'id' => $event->id(),
          'reset' => 1,
        ],
      ])->toString();

      // Add event address.
      if (($address = roundearth_events_address_for_event($event, TRUE)) && (is_string($address))) {
        $variables['content']['address'] = check_markup($address);

        // Add map to template if enabled.
        if (($event->hasField('is_map')) && ($is_map = $event->get('is_map')->value)) {
          $variables['content']['is_map'] = $is_map;

          // TODO: Use CiviCRM geocoding API to embed the map.
          // For now, embed with google maps.
          $variables['content']['google_map'] = [
            '#type' => 'inline_template',
            '#template' => '<iframe src="https://www.google.com/maps?q={{ address }}&output=embed" width="100%" height="500px" frameborder="0"></iframe>',
            '#context' => [
              'address' => $address,
            ],
          ];
        }
      }

      // Add location link.
      if (($event->hasField('field_roundearth_events_loc_link')) && ($uri = $event->get('field_roundearth_events_loc_link')->uri)) {
        $variables['content']['location_link'] = Url::fromUri($uri);
      }

      // Add share links.
      if (($event->hasField('is_share')) && ($is_share = $event->get('is_share')->value)) {
        $variables['content']['is_share'] = $is_share;

        // Generate a share link to this event page.
        $variables['content']['event_url'] = $event->toUrl('canonical', ['absolute' => TRUE]);

        // Generate an ical link.
        $variables['content']['ical_url'] = Url::fromRoute('civicrm.civicrm_event_ical', [], [
          'query' => [
            'id' => $event->id(),
            'reset' => 1,
          ],
        ])->toString();

        // Create share links for social media.
        $variables['content']['social_media_links'] = roundearth_events_get_social_media_links_for_event($event);
      }

      // Registration links.
      if ($event->hasField('is_monetary') && ($is_monetary = $event->get('is_monetary')->value)) {
        $variables['content']['is_monetary'] = $is_monetary;

        // Get the currency.
        if ($event->hasField('currency')) {
          $variables['content']['currency'] = $event->get('currency')->value;
        }

        // Get event prices.
        if ($prices = roundearth_events_get_prices_for_event($event)) {
          $variables['content']['prices'] = $prices;
        }
      }
      break;
  }
}
