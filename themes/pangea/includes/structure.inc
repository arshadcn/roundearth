<?php
/**
 * @file
 * Theme and preprocess functions for breadcrumbs, messages, tabs..etc
 */


/**
 * Implements template_preprocess_breadcrumb().
 */
function pangea_preprocess_breadcrumb(&$variables) {
  // No breadcrumb if only 1 link.
  if (count($variables['breadcrumb']) <= 1) {
    $variables['breadcrumb'] = [];
  }
}
