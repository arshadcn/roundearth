<?php

/**
 * Implements template_preprocess_paragraph().
 */
function pangea_preprocess_paragraph(&$variables) {
  /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
  $paragraph = $variables['elements']['#paragraph'];

  switch ($paragraph->bundle()) {
    case 'roundearth_events_session':
      // Add the speakers to the template.
      if (($paragraph->hasField('field_roundearth_events_speaker')) && ($speakers = $paragraph->get('field_roundearth_events_speaker')->referencedEntities())) {
        $view_builder = Drupal::entityTypeManager()->getViewBuilder('civicrm_contact');
        foreach ($speakers as $key => $speaker) {
          $variables['content']['field_roundearth_events_speaker'][$key] = $view_builder->view($speaker);
        }
      }
      break;

    case 'roundearth_events_speaker':
      // Make the speaker entity reachable in the template.
      /** @var \Drupal\civicrm_entity\Entity\CivicrmEntity $contact */
      if (($paragraph->hasField('field_roundearth_events_spk')) && ($contact = $paragraph->get('field_roundearth_events_spk')->entity)) {
        // Get fields from civicrm_contact.
        $contact_fields = [
          'image_URL',
          'display_name',
          'job_title',
          'current_employer',
        ];

        foreach ($contact_fields as $field) {
          if (($contact->hasField($field)) && ($value = $contact->get($field)->value)) {
            $variables['content']['contact'][$field] = $value;
          }
        }

        // Add bio (custom civicrm field).
        if ($bio = roundearth_events_get_custom_field_value_for_entity('roundearth_events_bio', $contact)) {
          $variables['content']['contact']['bio'] = [
            '#markup' => $bio,
          ];
        }
      }

      // Add affiliation to template.
      if (($paragraph->hasField('field_roundearth_events_spkafltn')) && ($affiliation = $paragraph->get('field_roundearth_events_spkafltn')->value)) {
        $variables['content']['affiliation'] = $affiliation;
      }
      break;
  }
}
